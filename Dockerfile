
## start with the Docker 'base R' Debian-based image
FROM rocker/r-devel:latest

RUN apt-get update -qq \
    && apt-get dist-upgrade -y \
    && apt-get install -t unstable -y --no-install-recommends \
        r-cran-roxygen2 \
        libxml2-dev \
        libssl-dev \
        r-cran-knitr \
        r-cran-rmarkdown \
        r-cran-ggplot2 \
        r-cran-testthat \
        r-cran-devtools \
        qpdf \
        pandoc \
        pandoc-citeproc \
        libgmp3-dev \
        libmpfr-dev \
        libmpfr-doc \
    && RD -e 'install.packages(c("devtools","roxygen2","callr","rvest","xml2","tidyverse","gmp","Rmpfr","knitr","rmarkdown","ggplot2","qpdf"),quiet=TRUE)'
